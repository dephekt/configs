#!/usr/bin/env bash

# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=10000
HISTFILESIZE=20000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
        # We have color support; assume it's compliant with Ecma-48
        # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
        # a case would tend to support setf rather than setaf.)
        color_prompt=yes
    else
        color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Source the Platform S3 release archive credentials if they exist.
if [ -r /etc/psh_s3_ra ]; then
  . /etc/psh_s3_ra
fi

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -r ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -r /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -r /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    pathmunge "$HOME/.local/bin"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    pathmunge "$HOME/bin"
fi

# check if ~/.pyenv exists, then set PYENV_ROOT and initialize pyenv
# shims and enable autocompletion. add to PATH if necessary.
if [ -d "$HOME/.pyenv" ]; then
    export PYENV_ROOT="$HOME/.pyenv"
    command -v pyenv >/dev/null || pathmunge "$PYENV_ROOT/bin"
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"
fi

# Use the gpg-agent SSH socket, requires scdaemon to be installed
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/gnupg/S.gpg-agent.ssh"

# Set the TTY for GPG
GPG_TTY="$(tty)"
export GPG_TTY

# Add psssh to the shell prompt if there's an active PSSSH token
[ -n "$PSSSH_ACCESS_TOKEN" ] && PS1="(psssh) $PS1"

# Use the Ubuntu keyserver for LXC image downloads
export DOWNLOAD_KEYSERVER="hkps://keyserver.ubuntu.com"

# BEGIN SNIPPET: Platform.sh CLI (Staging) configuration
HOME=${HOME:-"/home/dephekt"}
export PATH="$HOME/"'.platformsh-stg/bin':"$PATH"
if [ -r "$HOME/.platformsh-stg/shell-config.rc" ]; then . "$HOME/.platformsh-stg/shell-config.rc"; fi # END SNIPPET

# If we have a rust cargo env then source it
if [ -r "$HOME/.cargo/env" ]; then . "$HOME/.cargo/env"; fi

# If nvidia-vaapi-driver is installed, set environment so Firefox uses NVDEC as its VA-API backend
if [ -r /usr/lib64/dri/nvidia_drv_video.so ] || [ -r /usr/lib/x86_64-linux-gnu/dri/nvidia_drv_video.so ]; then
    export LIBVA_DRIVER_NAME=nvidia
    export MOZ_DISABLE_RDD_SANDBOX=1
fi

# Set PATH so it includes user's local go bin if it exists
# and set GOROOT
if [ -d /usr/local/go/bin ]; then
    export GOROOT="/usr/local/go"
    pathmunge "/usr/local/go/bin"
fi

# Set GOPATH if $HOME/go exists
if [ -d "$HOME/go" ]; then
    export GOPATH="$HOME/go"
fi

# Added by Toolbox App
export PATH="$PATH:$HOME/.local/share/JetBrains/Toolbox/scripts"

# Check for Platform.sh VPN cert expiry
if ! openssl x509 -noout -enddate -checkend 259200 -in "$HOME"/.cert/nm-openvpn/Platform*-cert.pem >/dev/null; then
    echo "$(tput blink)$(tput setaf 1)Note:$(tput sgr0) Your VPN certificate will expire in less than 3 days. Please go to https://certifier.pltfrm.sh/openvpn to get a new one."
fi

if ! openssl x509 -noout -enddate -checkend 259200 -in "$HOME"/.cert/nm-openvpn/Platform*-ca.pem >/dev/null; then
    echo "$(tput blink)$(tput setaf 1)Note:$(tput sgr0) Your VPN CA certificate will expire in less than 3 days. Please go to https://certifier.pltfrm.sh/openvpn to get a new one. If this continues, alert ops."
fi

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

export VIRTUALENV_DISABLE_PROMPT=1
export PYENV_VIRTUALENV_DISABLE_PROMPT=1

function omg_prompt_callback() {
    if [ -n "${VIRTUAL_ENV}" ]; then
        echo "\e[0;35m(`basename ${VIRTUAL_ENV}`)\e[0m "
    fi
}

#. "$HOME"/.oh-my-git/prompt.sh

omg_is_a_git_repo_symbol=''
omg_has_untracked_files_symbol=''
omg_has_adds_symbol=''
omg_has_deletions_symbol=''
omg_has_cached_deletions_symbol=''
omg_has_modifications_symbol='󰏫'
omg_has_cached_modifications_symbol='󰷈'
omg_ready_to_commit_symbol='󰗼'
omg_is_on_a_tag_symbol=''
omg_needs_to_merge_symbol='ᄉ'
omg_detached_symbol=''
omg_can_fast_forward_symbol='󰄿'
omg_has_diverged_symbol=''
omg_not_tracked_branch_symbol='󰌢'
omg_rebase_tracking_branch_symbol=''
omg_merge_tracking_branch_symbol=''
omg_should_push_symbol='' # 󰭾'
omg_has_stashes_symbol=''
# >>> xmake >>>
test -f "/home/daniel/.xmake/profile" && source "/home/daniel/.xmake/profile"
# <<< xmake <<<