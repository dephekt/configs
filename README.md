# Dotfiles

Just my config files

## Repo Setup

First, you should probably execute the [Ansible playbook](https://gitlab.com/dephekt/workstation-playbook) to ensure dependencies are already available and that the system is setup and configured in a sane way for work.

Then run the following script to setup the dotfiles:

```bash
curl -Ls https://gitlab.com/dephekt/configs/raw/master/.bin/install.sh | /bin/bash
```

That's all. The config files should all be available now.
