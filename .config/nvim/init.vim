syntax on

filetype plugin indent on

set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4

set noswapfile
"set colorcolumn=80

set lcs=tab:>~,trail:~ list

set number

set path+=**

set wildmenu

set smartindent

set ic

" Use the below highlight group when displaying bad whitespace is desired.
highlight BadWhitespace ctermbg=red guibg=red

" Display tabs at the beginning of a line in Python mode as bad.
au BufRead,BufNewFile *.py,*.pyw match BadWhitespace /^\t\+/
" Make trailing whitespace be flagged as bad.
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/

call plug#begin()

Plug 'preservim/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'neoclide/coc.nvim', {'branch': 'release'}

call plug#end()


autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists('s:std_in') | NERDTree | endif
let NERDTreeChDirMode=1
let NERDTreeIgnore=['\.pyc$', '\~$'] "ignore files in NERDTree
nnoremap <C-t> :NERDTreeFocus<CR>

vnoremap <leader>d c<c-r>=system('base64 --decode', @")<cr><esc>
vnoremap <leader>e y:let @z=system('echo -n ' . shellescape(@") . ' \| base64 -w 0')<cr>gvd"zP

