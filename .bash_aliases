#!/usr/bin/env bash

pathmunge() {
  case ":${PATH}:" in
  *:"$1":*) ;;

  *)
    if [ "$2" = "after" ]; then
      PATH=$PATH:$1
    else
      PATH=$1:$PATH
    fi
    ;;
  esac
}

# Pipe diff output to diff-so-fancy
dsf() {
  diff -u "$@" | diff-so-fancy -
}

# Provide a dotfiles repo git alias
dotfiles() {
  $(which git) --git-dir="$HOME"/.cfg/ --work-tree="$HOME" "$@"
}

manufacture() {
  $(which docker) run --rm -it -w /mnt -v "$PWD":/mnt -e "AWS_ACCESS_KEY_ID=${PSH_S3_RA_ACCESS}" -e "AWS_SECRET_ACCESS_KEY=${PSH_S3_RA_SECRET}" --privileged registry.lab.plat.farm/images/gitlab-ci-build:latest manufacture --rc-dir /mnt/cache/manufacture "$@"
}

container2image() {
  $(which docker) run --rm -it -w /mnt -v "$PWD":/mnt -e "LC_ALL=C.UTF-8" -e "LANG=C.UTF-8" -e "AWS_ACCESS_KEY_ID=${PSH_S3_RA_ACCESS}" -e "AWS_SECRET_ACCESS_KEY=${PSH_S3_RA_SECRET}" --privileged registry.lab.plat.farm/images/gitlab-ci-build:latest container2image "$@"
}

ci-build-manufacture-image() {
  $(which docker) run --rm -it -w /mnt -v "$PWD":/mnt -e "LC_ALL=C.UTF-8" -e "LANG=C.UTF-8" -e "AWS_ACCESS_KEY_ID=${PSH_S3_RA_ACCESS}" -e "AWS_SECRET_ACCESS_KEY=${PSH_S3_RA_SECRET}" --privileged registry.lab.plat.farm/images/ci-build-container:latest ci-build-manufacture-image "$@"
}

release() {
  $(which docker) run --rm -it -v ~/.gitconfig:/root/.gitconfig -v "$PWD"/release-archive:/release-archive -v "$SSH_AUTH_SOCK":/ssh-agent --env SSH_AUTH_SOCK=/ssh-agent registry.lab.plat.farm/images/ci-build-container:latest release "$@"
}

testbed-run() {
  TESTBED_DIR=$HOME/Platformsh/foundation/testbed
  SERVICES_DIR=$HOME/Platformsh/workspace-services/services
  sudo bash -c "source $HOME/.pyenv/versions/testbed/bin/activate && screen -S testbed testbed $TESTBED_DIR/configs/default.yml --http 80 --no-exit-on-error --services-path $SERVICES_DIR $*"
}

review() {
  mkdir -p /tmp/python-review-cache
  $(which docker) run --rm --tty -w /src -v "$PWD":/src -e LC_ALL=C.UTF-8 -e LANG=C.UTF-8 -v /tmp/python-review-cache:/tmp/.cache -e XDG_CACHE_HOME=/tmp/.cache --user "$(id -u)":"$(id -g)" registry.lab.plat.farm/images/python-review:latest "$@"
}

pyl() {
  review pyl --python-version python3 "$@"
}

ci-build-debian-package() {
  $(which docker) run --rm -it registry.lab.plat.farm/images/ci-build-container:latest ci-build-debian-package "$@"
}

influx() {
  $(which docker) run --rm -it --net=host -v /tmp/influx:/tmp registry.lab.plat.farm/images/influxdb:1.8-228 influx "$@"
}

instance_to_ip() {
  if [ -z "$1" ] || [ -z "$2" ]; then
    echo "Usage: ${FUNCNAME[0]} <instance_name> [shell]"
    return 1
  fi

  COORD=$1
  HOST=$2

  IP=$(echo "print(next(iter([h.ip for h in root.hosts.itervalues() if \"$HOST\" in h.__name__])))" | psssh @"$1" platform script -)

  if [ "$3" = "shell" ]; then
    psssh -p444 -J @"$COORD":444 "$(psssh token)"@"$IP"
  else
    echo "$IP"
  fi
}

curld() { curl --write-out @"$HOME"/.curl_format --output /dev/null --dump-header /dev/stdout --location --silent "$@"; }

with-testbed-upsun() {
  UPSUN_CLI_SESSION_ID=testbed \
  UPSUN_CLI_API_URL=http://testbed.plat.farm \
  UPSUN_CLI_OAUTH2_AUTH_URL=http://testbed.plat.farm/oauth2/authorize \
  UPSUN_CLI_OAUTH2_TOKEN_URL=http://testbed.plat.farm/oauth2/token \
  UPSUN_CLI_OAUTH2_REVOKE_URL=http://testbed.plat.farm/oauth2/revoke \
  UPSUN_CLI_SSH_DOMAIN_WILDCARD="*.plat.farm" \
  UPSUN_CLI_SKIP_SSL=1 \
  UPSUN_CLI_TOKEN=mytoken \
  UPSUN_CLI_API_ORGANIZATIONS=0 \
  UPSUN_CLI_AUTO_LOAD_SSH_CERT=0 \
  UPSUN_CLI_CERTIFIER_URL="" \
  "$@"
}

with-testbed() {
  PLATFORMSH_CLI_SESSION_ID=testbed \
  PLATFORMSH_CLI_API_URL=http://testbed.plat.farm \
  PLATFORMSH_CLI_OAUTH2_AUTH_URL=http://testbed.plat.farm/oauth2/authorize \
  PLATFORMSH_CLI_OAUTH2_TOKEN_URL=http://testbed.plat.farm/oauth2/token \
  PLATFORMSH_CLI_OAUTH2_REVOKE_URL=http://testbed.plat.farm/oauth2/revoke \
  PLATFORMSH_CLI_SSH_DOMAIN_WILDCARD="*.plat.farm" \
  PLATFORMSH_CLI_SKIP_SSL=1 \
  PLATFORMSH_CLI_TOKEN=mytoken \
  PLATFORMSH_CLI_API_ORGANIZATIONS=0 \
  PLATFORMSH_CLI_AUTO_LOAD_SSH_CERT=0 \
  PLATFORMSH_CLI_CERTIFIER_URL="" \
  "$@"
}

battery_consumption() {
  CURRENT=$(cat /sys/class/power_supply/BAT0/current_now)
  VOLTAGE=$(cat /sys/class/power_supply/BAT0/voltage_now)
  µWATTS=$(( CURRENT * VOLTAGE ))
  WATTS=$(( µWATTS / 1000000000000 ))
  echo - | awk "{ printf \"%.1f\", $WATTS }"; echo " W"
}

alias dl="aria2c"
